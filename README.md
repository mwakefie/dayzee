# Dayzee #

Dayzee is a location-based app that uses your iPhone's built-in GPS to show you nearby locations to purchase flower arrangements.  Simply load the app, allow the app access your device's location, and florists in your area appear right before your eyes!

![preview.png](https://bitbucket.org/repo/RoMgGe/images/3342550311-preview.png)
## What is this repository for? ##

* This is sample app used to demonstrate Objective C functionality.
* Version: 1.0

## What do I need? ##

This project can be built using XCode 5.1.1.  The app is currently optimized for retina-capable iPhones running iOS 7.  

## Who do I talk to? ##

* Owner: matt@wakefieldweb.net