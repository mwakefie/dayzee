//
//  DayzeeViewController.h
//  Dayzee
//
//  Created by Matthew Wakefield on 8/24/14.
//  Copyright (c) 2014 MVW Technologies.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface DayzeeViewController : UIViewController<CLLocationManagerDelegate, MKMapViewDelegate>

// The location manager used to determine device location.
@property (nonatomic, retain) CLLocationManager *locationManager;

@end
