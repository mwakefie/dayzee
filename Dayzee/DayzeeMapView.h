//
//  DayzeeMapView.h
//  Dayzee
//
//  Created by Matthew Wakefield on 8/26/14.
//  Copyright (c) 2014 MVW Technologies.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface DayzeeMapView : MKMapView

// Plots points on map given a search object.
- (void)plotPointsFromSearch:(MKLocalSearch *)search;

@end