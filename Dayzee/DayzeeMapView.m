//
//  DayzeeMapView.m
//  Dayzee
//
//  Created by Matthew Wakefield on 8/26/14.
//  Copyright (c) 2014 MVW Technologies.
//

#import "DayzeeMapView.h"

@implementation DayzeeMapView

- (void)plotPointsFromSearch:(MKLocalSearch *)search {
  [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
    
    // Check for empty response.
    if (response.mapItems.count == 0) {
      NSString *message = @"We're sorry - Dayzee was unable to locate any florists in your area.";
      UIAlertView * alert =[[UIAlertView alloc] initWithTitle:@"Problem"
                                                      message:message
                                                     delegate:self
                                            cancelButtonTitle:@"Dismiss"
                                            otherButtonTitles: nil];
      
      // Notify user with an alert that app was unable to locate any florists.
      [alert show];
    }
    else {
      
      // Remove existing pins.
      [self removeAnnotations:self.annotations];
      
      // Loop through response and add to annotations array.
      NSMutableArray *annotations = [NSMutableArray array];
      for (MKMapItem *item in response.mapItems) {
        
        // Add points to show.
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = item.placemark.coordinate;
        point.title = item.name;
        [annotations addObject:point];
      }
      
      // Show annotations on map along with user location.
      [self showAnnotations:annotations animated:YES];
      self.showsUserLocation = YES;
    }
  }];
}

@end