//
//  AppDelegate.h
//  Dayzee
//
//  Created by Matthew Wakefield on 8/24/14.
//  Copyright (c) 2014 MVW Technologies.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end