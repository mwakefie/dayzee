//
//  DayzeeViewController.h
//  Dayzee
//
//  Created by Matthew Wakefield on 8/24/14.
//  Copyright (c) 2014 MVW Technologies.
//

#import "DayzeeViewController.h"
#import "DayzeeMapView.h"

@interface DayzeeViewController() {
  
  // The sub-view containing map with plotted points, based on search.
  DayzeeMapView *mapView;
}

@end

@implementation DayzeeViewController

@synthesize locationManager;

// This method notifies the super-class and kicks off Dayzee mapping functions.
- (void)viewDidLoad {
  
  // Notify the super-class that the view did load and start mapping.
  [super viewDidLoad];
  [self startMapping];
}

// This method begins tracking device location, sets up the map, and sets up toolbar with mapping controls.
- (void)startMapping {
  
  // Begin tracking device location, setup map, and setup toolbar with mapping controls.
  [self beginLocationTracking];
  [self setupToolbar];
  [self setupMap];
}

// This method kicks off location tracking.
- (void)beginLocationTracking {
  
  // If not yet defined, initialize location manager with accuracy of nearest 10 meters.
  if (locationManager == nil) {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    locationManager.delegate = self;
  }
  
  // Begin collecting location information from location manger.
  [locationManager startUpdatingLocation];
}

// This method adds buttons to the toolbar for mapping purposes.
- (void)setupToolbar {
  
  // Add refresh button.
  UIButton *refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
  UIImage *image = [UIImage imageNamed:@"arrow"];
  [refreshButton setImage:image forState:UIControlStateNormal];
  [refreshButton setFrame:CGRectMake(0, 0, 19, 20)];
  [refreshButton addTarget:self action:@selector(doRefresh:) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:refreshButton];
  
  
  // Add information button.
  UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
	[infoButton addTarget:self action:@selector(showInfoView:) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
}

// This method adds a map as a sub-view and sets up horizontal and vertical constraints.
- (void)setupMap {
  
  // Initialize map and add as sub-view.
  mapView = [[DayzeeMapView alloc] init];
  mapView.delegate = self;
  [self.view addSubview:mapView];
  
  // Constrain view horizontally.
  NSArray *xBindings = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[mapView]|"
                                                               options:0 metrics:nil
                                                                 views:@{@"mapView": mapView}];
  // Constrain view vertically.
  NSArray *yBindings = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[mapView]|"
                                                               options:0 metrics:nil
                                                                 views:@{@"mapView": mapView}];
  // Add constraints to view.
  [self.view addConstraints:xBindings];
  [self.view addConstraints:yBindings];
  [mapView setTranslatesAutoresizingMaskIntoConstraints:NO];
}

// This method returns a customized annotation view (flower pin image) to display on the map.
- (MKAnnotationView *)mapView:(MKMapView *)mkMapView viewForAnnotation:(id<MKAnnotation>)annotation {

  // Don't use flower pin for user location dot.
  if ([annotation isKindOfClass:[MKUserLocation class]]) {
    return nil;
  }
  
  // Use custom image for location pins.
  NSString *identifier = @"CustomViewAnnotation";
  MKAnnotationView* annotationView = [mkMapView dequeueReusableAnnotationViewWithIdentifier:identifier];
  if (!annotationView) {
    annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                  reuseIdentifier:identifier];
  }
  
  // Use flower image with callout.
  annotationView.image = [UIImage imageNamed:@"flower"];
  annotationView.canShowCallout= YES;
  return annotationView;
}

// This method refreshes the map by initiating a new search based on the current device location.
- (void) doRefresh:(id)sender {
  
  // Get location again.
  [locationManager startUpdatingLocation];
}

// This method displays an "easter egg" to the user.
- (void) showInfoView:(id)sender {
  
  // Create message for user.
  NSString *message = @"Hello!";
  UIAlertView * alert =[[UIAlertView alloc] initWithTitle:@"Hello"
                                                  message:message
                                                 delegate:self
                                        cancelButtonTitle:@"Dismiss"
                                        otherButtonTitles: nil];
  // Show alert to user.
  [alert show];
}

// This method executes when device location is updated - it stops updating location and initiates new search.
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
  
  // Now we have our location - turn off the location manager to save power and initiate search.
  [manager stopUpdatingLocation];
  [self initiateSearchBasedOnLocation:[locations lastObject]];
}

// This method initiates a search for florists based on the current device location.
- (void)initiateSearchBasedOnLocation:(CLLocation *)location {
  
  // Zoom closer (within 1,000 meters) to user's location in map view while search is executing.
  CLLocationCoordinate2D locationCoordinate;
  locationCoordinate.latitude = location.coordinate.latitude;
  locationCoordinate.longitude= location.coordinate.longitude;
  MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(locationCoordinate, 1000, 1000);
  [mapView setRegion:region animated:NO];
  
  // Configure search request for flower vendor locations.
  MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
  request.naturalLanguageQuery = @"florist store";
  request.region = mapView.region;
  
  // Execute search for flower vendor locations.
  MKLocalSearch *search = [[MKLocalSearch alloc]initWithRequest:request];
  [mapView plotPointsFromSearch:search];
}

// This method displays a nice message notifying the user if device location cannot be determined.
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
  
  // Create message for user regarding device location capabilities.
  NSString *notification = @"Dayzee was unable to determine your location.";
  NSString *instructions = @"Please enable location services on your device to continue using Dayzee.";
  NSString *message = [NSString stringWithFormat:@"%@ %@", notification, instructions];
  
  // Notify the user with an alert that app was unable to determine device location.
  UIAlertView * alert =[[UIAlertView alloc] initWithTitle:@"Problem"
                                                  message:message
                                                 delegate:self
                                        cancelButtonTitle:@"Dismiss"
                                        otherButtonTitles: nil];
  // Show alert to user.
  [alert show];
}

// This method is called when the view will disappear - it turns off the location manager to save power.
- (void)viewWillDisappear:(BOOL)animated {
  
  // Notify the super-class that the view will disappear.
  [super viewWillDisappear:animated];
  
  // Turn off the location manager to save power.
  [self.locationManager stopUpdatingLocation];
}

@end