//
//  DayzeeViewControllerTests.m
//  Dayzee
//
//  Created by Matthew Wakefield on 8/29/14.
//  Copyright (c) 2014 MVW Technologies.
//

#import <XCTest/XCTest.h>

#import "AppDelegate.h"
#import "DayzeeViewController.h"
#import "DayzeeMapView.h"

@interface DayzeeViewControllerTests : XCTestCase {
  DayzeeViewController *viewController;
}

@end

@implementation DayzeeViewControllerTests

- (void)setUp {
  
  // Setup super class.
  [super setUp];
  
  // Get reference to view controller.
  AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
  viewController = [[DayzeeViewController alloc] init];
  delegate.window.rootViewController = viewController;
  [delegate.window makeKeyAndVisible];
}

- (void)tearDown {
  [super tearDown];
}

- (void)testStartMapping {
  
  // Ensure methods called by this method pass in the following order:
  [self testBeginLocationTracking];
  [self testSetupToolbar];
  [self testSetupMap];
}

- (void)testBeginLocationTracking {
  
  // Ensure location manager was initialized.
  XCTAssertNotNil(viewController.locationManager);
}

- (void)testSetupToolbar {
  
  // Ensure mapping buttons were added.
  XCTAssertNotNil(viewController.navigationItem.leftBarButtonItem);
  XCTAssertNotNil(viewController.navigationItem.rightBarButtonItem);
}

- (void)testSetupMap {
  
  // Ensure 3 sub-views were added - the map view and 2 contraints.
  XCTAssertNotNil(viewController.view.subviews);
  XCTAssertEqual(3, viewController.view.subviews.count);
}

- (void)testAnnotations {
  
  // Get map view.
  UIView *view = viewController.view.subviews[0];
  XCTAssertTrue([view isKindOfClass:[DayzeeMapView class]]);
  DayzeeMapView *mapView = (DayzeeMapView *)view;

  // Wait 5 seconds for test results.
  [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:5]];
  
  // Verify test results were returned.
  if (mapView.annotations.count > 0) {
    MKPointAnnotation *annotation = mapView.annotations[0];
    XCTAssertNotNil(annotation);
    NSString *title = annotation.title;
    XCTAssertNotNil(title);
    XCTAssertNotEqual(@"", title);
  }
  else {
    XCTFail();
  }
}

@end
