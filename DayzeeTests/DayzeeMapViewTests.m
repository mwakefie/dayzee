//
//  DayzeeMapViewTests.m
//  Dayzee
//
//  Created by Matthew Wakefield on 8/29/14.
//  Copyright (c) 2014 MVW Technologies.
//

#import <XCTest/XCTest.h>
#import <MapKit/MapKit.h>

#import "AppDelegate.h"
#import "DayzeeMapView.h"
#import "DayzeeViewController.h"

@interface DayzeeMapViewTests : XCTestCase {
  DayzeeMapView *mapView;
}

@end

@implementation DayzeeMapViewTests

- (void)setUp {
  
  // Setup super-class.
  [super setUp];
  
  // Get DayzeeMapView object.
  AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
  DayzeeViewController *viewController = [[DayzeeViewController alloc] init];
  delegate.window.rootViewController = viewController;
  [delegate.window makeKeyAndVisible];
  mapView = viewController.view.subviews[0];
}

- (void)tearDown {
  [super tearDown];
}

- (void)testPlotPointsFromSearch {
  
  // Configure request and execute search.
  MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
  request.naturalLanguageQuery = @"florist store";
  MKLocalSearch *search = [[MKLocalSearch alloc]initWithRequest:request];
  [mapView plotPointsFromSearch:search];
  
  // Wait 5 seconds for test results.
  [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:5]];

  // Verify test results were returned.
  if (mapView.annotations.count > 0) {
    MKPointAnnotation *annotation = mapView.annotations[0];
    XCTAssertNotNil(annotation);
    NSString *title = annotation.title;
    XCTAssertNotNil(title);
    XCTAssertNotEqual(@"", title);
  }
  else {
    XCTFail();
  }
}

@end
